<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script src="https://use.fontawesome.com/ef3af1389e.js"></script>
    <title>@yield('title')</title>
</head>
<body>
<div class="container-fluid p-0">
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <!-- Brand -->
        <a class="navbar-brand" href="#">@yield('title')</a>
        <!-- Links -->
        <ul class="navbar-nav">
            <!-- Dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Créateurs
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('createurs') }}">Index</a>
                    <a class="dropdown-item" href="{{ route('createurs.create') }}">Ajouter</a>
                </div>
            </li>
            <!-- Dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Stocks
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('stocks') }}">Index</a>
                    <a class="dropdown-item" href="{{ route('stocks.create') }}">Ajouter</a>
                </div>
            </li>
            <!-- Dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Salles
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('salles') }}">Index</a>
                    <a class="dropdown-item" href="{{ route('salles.create') }}">Ajouter</a>
                </div>
            </li>
            <!-- Dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Objets
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('objets') }}">Index</a>
                </div>
            </li>
        </ul>
    </nav>
    <div class="container-fluid p-5">
        @yield('content')
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="{{ asset('js/function.js') }}"></script>
</body>
</html>
