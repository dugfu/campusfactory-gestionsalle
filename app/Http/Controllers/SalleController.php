<?php

namespace App\Http\Controllers;

use App\Createur;
use App\Salle;
use App\Stock;
use Illuminate\Http\Request;

class SalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Salles\salle', ['salles' => Salle::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Salles\create', ['createurs' => Createur::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salle = new Salle();
        $salle->idCreateur = $request->input('idCreateur');
        $salle->nom = $request->input('nom');
        if ($request->input('longueur') > $request->input('largeur')) {
            $salle->largeur = $request->input('largeur');
            $salle->longueur = $request->input('longueur');
        }else{
            $salle->largeur = $request->input('longueur');
            $salle->longueur = $request->input('largeur');
        }
        $salle->save();
        return redirect()->route('salles', ['boolinsert' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function show(Salle $salle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Salles\edit', ['salle' => Salle::findOrFail($id), 'createurs' => Createur::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $salle = Salle::findOrFail($id);
        $salle->nom = $request->input('nom');
        if ($request->input('longueur') > $request->input('largeur')) {
            $salle->largeur = $request->input('largeur');
            $salle->longueur = $request->input('longueur');
        }else{
            $salle->largeur = $request->input('longueur');
            $salle->longueur = $request->input('largeur');
        }
        $salle->idCreateur = $request->input('idCreateur');
        $salle->save();
        return redirect()->route('salles', ['boolupdate' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        return view('Salles\suppr', ['salle' => Salle::findOrFail($id), 'createurs' => Createur::all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salle = Salle::findOrFail($id);
        $salle->delete();
        return redirect()->route('salles', ['booldelete' => true]);
    }

    public function configurate($id)
    {
        return view('Salles\configure', ['salle' => Salle::findOrFail($id), 'stocks' => Stock::all()]);
    }
}
