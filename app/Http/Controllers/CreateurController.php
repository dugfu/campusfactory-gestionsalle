<?php

namespace App\Http\Controllers;

use App\Createur;
use Illuminate\Http\Request;

class CreateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Createurs\createur', ['createurs' => Createur::all(), 'boolinsert' => false]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Createurs\create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createur = new Createur();
        $createur->nom = $request->input('nom');
        $createur->prenom = $request->input('prenom');
        $createur->pseudo = $request->input('pseudo');
        $createur->save();
        return redirect()->route('createurs', ['boolinsert' => true]);
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function show(Createur $createur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Createurs\edit', ['createur' => Createur::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $createur = Createur::findOrFail($id);
        $createur->nom = $request->input('nom');
        $createur->prenom = $request->input('prenom');
        $createur->pseudo = $request->input('pseudo');
        $createur->save();
        return redirect()->route('createurs', ['boolupdate' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        return view('Createurs\suppr', ['createur' => Createur::findOrFail($id)]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $createur = Createur::findOrFail($id);
        $createur->delete();
        return redirect()->route('createurs', ['booldelete' => true]);
    }
}
