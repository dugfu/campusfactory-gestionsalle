@extends('layout')

@section('title','Stocks')

@section('content')
    <form method="post" action="{{ route('stocks.destroy', ['id' => $stock->id]) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" name="nom" class="form-control" placeholder="Entrer nom" id="nom" value="{{ $stock->nom }}" disabled>
        </div>
        <div class="form-group">
            <label for="longueur">Longueur (en mètres):</label>
            <input type="number" name="longueur" class="form-control" placeholder="Entrer longueur (m)" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" id="longueur" value="{{ $stock->longueur }}" disabled>
        </div>
        <div class="form-group">
            <label for="largeur">Largeur (en mètres):</label>
            <input type="number" name="largeur" class="form-control" placeholder="Entrer largeur (m)" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" id="largeur" value="{{ $stock->largeur }}" disabled>
        </div>
        <div class="form-check-inline">
            <label class="form-check-label">
                <input
                        type="checkbox"
                        class="form-check-input"
                        name="place"
                        value="1"
                        @if ($stock->place == true)
                        checked
                        @endif
                        disabled
                >Place

            </label>
        </div>
        <div class="container-fluid">
            <p>
                Êtes-vous sur de vouloir supprimer cet objet ?<br/>
                <button type="submit" class="btn btn-danger">Supprimer</button>
                <a class="btn btn-primary" href="{{ route('stocks') }}">Annuler</a>
            </p>
        </div>
    </form>
@endsection
