<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idSalle');
            $table->string('nom');
            $table->unsignedInteger('largeur');
            $table->unsignedInteger('longueur');
            $table->unsignedInteger('rotation');
            $table->unsignedInteger('positionX');
            $table->unsignedInteger('positionY');
            $table->boolean('place');
            $table->string('SVG');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objets');
    }
}
