const SVGLink_NS = 'http://www.w3.org/1999/xlink';
const SVG_NS = 'http://www.w3.org/2000/svg';
const svg = document.querySelector("#salleSVG");
const deg = 180 / Math.PI;
let rotating = false;
let dragging = false;
let impact = {
    x: 0,
    y: 0
};
let m = { //mouse
    x: 0,
    y: 0
};
delta = {
    x: 0,
    y: 0
};
const ry = []; // elements array
const objectsRy = [];

function Element(o, index) {
    this.g = document.createElementNS(SVG_NS, 'g');
    this.g.setAttributeNS(null, 'id', index);
    svg.appendChild(this.g);

    o.parent = this.g;

    this.el = drawElement(o);
    this.a = 0;
    this.tagName = o.tagName;
    this.elRect = this.el.getBoundingClientRect();
    this.svgRect = svg.getBoundingClientRect();
    this.Left = this.elRect.left - this.svgRect.left;
    this.Right = this.elRect.right - this.svgRect.left;
    this.Top = this.elRect.top - this.svgRect.top;
    this.Bottom = this.elRect.bottom - this.svgRect.top;

    this.LT = {
        x: this.Left,
        y: this.Top
    };
    this.RT = {
        x: this.Right,
        y: this.Top
    };
    this.LB = {
        x: this.Left,
        y: this.Bottom
    };
    this.RB = {
        x: this.Right,
        y: this.Bottom
    };
    this.c = {
        x: 0, //(this.elRect.width / 2) + this.Left,
        y: 0 //(this.elRect.height / 2) + this.Top
    };
    this.o = {
        x: o.pos.x,
        y: o.pos.y
    };

    this.A = Math.atan2(this.elRect.height / 2, this.elRect.width / 2);

    this.pointsValue = function() { // points for the box
        return (this.Left + "," + this.Top + " " + this.Right + "," + this.Top + " " + this.Right + "," + this.Bottom + " " + this.Left + "," + this.Bottom + " " + this.Left + "," + this.Top);
    }

    const box = {
        properties: {
            points: this.pointsValue(),
            fill: 'none',
            stroke: 'dodgerblue',
            'stroke-dasharray': '5,5'
        },
        parent: this.g,
        tagName: 'polyline'
    }
    this.box = drawElement(box);

    const leftTop = {
        properties: {
            cx: this.LT.x,
            cy: this.LT.y,
            r: 6,
            fill: "blue"
        },
        parent: this.g,
        tagName: 'circle'
    }

    this.lt = drawElement(leftTop);

    this.update = function() {
        let transf = 'translate(' + this.o.x + ', ' + this.o.y + ')' + ' rotate(' + (this.a * deg) + ')';
        this.el.setAttributeNS(null, 'transform', transf);
        this.box.setAttributeNS(null, 'transform', transf);
        this.lt.setAttributeNS(null, 'transform', transf);
    }

}

for (let i = 0; i < objectsRy.length; i++) {
    const el = new Element(objectsRy[i], i + 1);
    el.update();
    ry.push(el)
}

// EVENTS

svg.addEventListener("mousedown", function(evt) {

    let index = parseInt(evt.target.parentElement.id) - 1;
    console.log(ry[index].A * deg)
    if (evt.target.tagName == ry[index].tagName) {
        dragging = index + 1;
        impact = oMousePos(svg, evt);
        delta.x = ry[index].o.x - impact.x;
        delta.y = ry[index].o.y - impact.y;
    }

    if (evt.target.tagName == "circle") {
        rotating = parseInt(evt.target.parentElement.id);
    }

}, false);

svg.addEventListener("mouseup", function(evt) {
    rotating = false;
    dragging = false;
}, false);

svg.addEventListener("mouseleave", function(evt) {
    rotating = false;
    dragging = false;
}, false);

svg.addEventListener("mousemove", function(evt) {
    m = oMousePos(svg, evt);

    if (dragging) {
        var index = dragging - 1;
        ry[index].o.x = m.x + delta.x;
        ry[index].o.y = m.y + delta.y;
        ry[index].update();
    }

    if (rotating) {
        var index = rotating - 1;
        ry[index].a = Math.atan2(ry[index].o.y - m.y, ry[index].o.x - m.x) - ry[index].A;
        ry[index].update();
    }
}, false);

// HELPERS

function oMousePos(svg, evt) {
    let ClientRect = svg.getBoundingClientRect();
    return { //objeto
        x: Math.round(evt.clientX - ClientRect.left),
        y: Math.round(evt.clientY - ClientRect.top)
    }
}

function drawElement(o) {
    /*
    var o = {
      properties : {
      x1:100, y1:220, x2:220, y2:70},
      parent:document.querySelector("svg"),
      tagName:'line'
    }
    */
    var el = document.createElementNS(SVG_NS, o.tagName);
    for (var name in o.properties) {
        console.log(name);
        if (o.properties.hasOwnProperty(name)) {
            el.setAttributeNS(null, name, o.properties[name]);
        }
    }
    o.parent.appendChild(el);
    return el;
}

function ShowProperty(object, type = 0) {
    let stock;
    if (!object.nom) {
        stock = JSON.parse(object.getAttribute('dataProperty'));
    }
    const nom = $('<div/>');
    const longueur = $('<div/>');
    const largeur = $('<div/>');
    const place = $('<div/>');
    const input_X = $('<div/>');
    const input_Y = $('<div/>');
    const angle = $('<div/>');
    if(type == 0 || type == 1) {
        if (stock.nom) nom.html('<u>Nom :</u>&nbsp;' + stock.nom);
        if (stock.longueur) longueur.html('<u>Longueur :</u>&nbsp;' + stock.longueur + 'm');
        if (stock.largeur) largeur.html('<u>Largeur :</u>&nbsp;' + stock.largeur + 'm');
        if (stock.place) place.html('<u>Place :</u>&nbsp;' + stock.place);
        $('#property').html('');
        $('#property').append(nom);
        $('#property').append(longueur);
        $('#property').append(largeur);
        $('#property').append(place);    }
    if(type == 1) {
        // console.log(object.transform);
        input_X.html('<u>X :</u>&nbsp;<input class="input-pos" oninput="changePos(this,`X`,`' + stock.nom + '`)" type="number" value="' + object.transform.baseVal[0].matrix.e + '"/>');
        input_Y.html('<u>Y :</u>&nbsp;<input class="input-pos" oninput="changePos(this,`Y`,`' + stock.nom + '`)" type="number" value="' + object.transform.baseVal[0].matrix.f + '"/>');
        angle.html('<u>Rotation :</u>&nbsp;<input class="input-pos" oninput="changeAngle(this, `' + stock.nom + '`)" type="number" value="' + object.transform.baseVal[1].angle + '"/>')
        $('#property').append(input_X);
        $('#property').append(input_Y);
        $('#property').append(angle);
    }
}

function insertObject(longueur, largeur, object) {
    const id = object.nom + "-" + makeid(10);
    const rect = {
        properties: {
            fill: 'url(#trama)',
            d: RoundedRect(-(longueur / 2),-(largeur / 2),longueur,largeur,0),
            onclick: 'ShowProperty(this,1)',
            id: id,
            dataProperty: '{"nom": "' + id
                            + '", "longueur": "' + object.longueur
                            + '", "largeur": "' + object.largeur
                            + '", "place": "' + object.place
                            + '", "idObject": "' + object.id + '"}'
        },
        tagName: 'path',
        pos: {
            x: 50,
            y: 50
        }
    };

    objectsRy.push(rect);
    const el = new Element(objectsRy[objectsRy.length - 1], objectsRy.length);
    el.update();
    ry.push(el);

}

function RoundedRect(x, y, width, height, radius) {
    return "M" + x + "," + y
        + "h" + (width - radius)
        + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
        + "v" + (height - 2 * radius)
        + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
        + "h" + (radius - width)
        + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + -radius
        + "v" + -(height - 2 * radius)
        + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
        + "z";
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function changePos(field, axe, id){
    let elem;
    for( let el of ry) {
        if( el.el.id == id){
            elem = el;
            break;
        }
    }
    if (axe == 'X') {
       elem.o.x = field.value;
    }
    if (axe == 'Y') {
       elem.o.y = field.value;
    }
    elem.update();
}

function changeAngle(field, id){
    let elem;
    for( let el of ry) {
        if( el.el.id == id){
            elem = el;
            break;
        }
    }
    // console.log(elem);
    let val = field.value * Math.PI/180;
    elem.a = val;
    elem.update();
}
function saveSalle(idSalle){
    let xhr = new XMLHttpRequest();
    let formData = new FormData();
    
}
