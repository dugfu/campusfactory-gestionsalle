@extends('layout')

@section('title','Salles')

@section('content')

    <table class="table table-dark table-striped p-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>IDCréateur</th>
            <th>Nom</th>
            <th>Longueur</th>
            <th>Largeur</th>
            <th>Configurer</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($salles as $salle)
            <tr>
                <td>{{ $salle->id }}</td>
                <td>{{ $salle->idCreateur }}</td>
                <td>{{ $salle->nom }}</td>
                <td>{{ $salle->longueur }}</td>
                <td>{{ $salle->largeur }}</td>
                <td>
                    <a href="{{ route('salles.configurate', ['id' => $salle->id]) }}" class="btn btn-success" style="cursor: pointer;">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <a href="{{ route('salles.edit', ['id' => $salle->id]) }}" class="btn btn-primary" style="cursor: pointer;">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <a href="{{ route('salles.delete', ['id' => $salle->id]) }}" class="btn btn-danger" style="cursor: pointer;">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if (app('request')->input('boolinsert'))
        La salle a été ajouté.
    @endif
    @if (app('request')->input('boolupdate'))
        La salle a été modifié.
    @endif
    @if (app('request')->input('booldelete'))
        La salle a été supprimé.
    @endif
@endsection

