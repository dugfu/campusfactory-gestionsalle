@extends('layout')

@section('title','Stocks')

@section('content')
    <form method="post" action="{{ route('stocks.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" name="nom" class="form-control" placeholder="Entrer nom" id="nom" required>
        </div>
        <div class="form-group">
            <label for="longueur">Longueur (en mètres):</label>
            <input type="number" name="longueur" class="form-control" placeholder="Entrer longueur (m)" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" id="longueur" required>
        </div>
        <div class="form-group">
            <label for="largeur">Largeur (en mètres):</label>
            <input type="number" name="largeur" class="form-control" placeholder="Entrer largeur (m)" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" id="largeur" required>
        </div>
        <div class="form-check-inline">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input" name="place" value="1" >Place
            </label>
        </div>
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
    </form>
@endsection

