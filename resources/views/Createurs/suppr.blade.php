@extends('layout')

@section('title','Créateurs')

@section('content')
    <form method="post" action="{{ route('createurs.destroy', ['id' => $createur->id]) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" name="nom" class="form-control" placeholder="Entrer nom" id="nom" value="{{ $createur->nom }}" disabled>
        </div>
        <div class="form-group">
            <label for="prenom">Prénom :</label>
            <input type="text" name="prenom" class="form-control" placeholder="Entrer prénom" id="prenom" value="{{ $createur->prenom }}" disabled>
        </div>
        <div class="form-group">
            <label for="pseudo">Pseudo :</label>
            <input type="text" name="pseudo" class="form-control" placeholder="Entrer pseudo" id="pseudo" value="{{ $createur->pseudo }}" disabled>
        </div>
        <div class="container-fluid">
            <p>
                Êtes-vous sur de vouloir supprimer ce créateur ?<br/>
                <button type="submit" class="btn btn-danger">Supprimer</button>
                <a class="btn btn-primary" href="{{ route('createurs') }}">Annuler</a>
            </p>
        </div>

    </form>

@endsection
