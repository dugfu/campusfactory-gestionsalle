<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Objet;
use Faker\Generator as Faker;

$factory->define(Objet::class, function (Faker $faker) {
    return [
        'nom'
    ];
});
