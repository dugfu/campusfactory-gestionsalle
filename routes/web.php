<?php
use App\Http\Controllers\CreateurController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::redirect('/','/createurs');

Route::get('/createurs', 'CreateurController@index')->name('createurs');
Route::get('/createurs/create', 'CreateurController@create')->name('createurs.create');
Route::post('/createurs/store', 'CreateurController@store')->name('createurs.store');
Route::get('/createurs/edit/{id}', 'CreateurController@edit')->name('createurs.edit');
Route::post('/createurs/update/{id}', 'CreateurController@update')->name('createurs.update');
Route::get('/createurs/delete/{id}', 'CreateurController@delete')->name('createurs.delete');
Route::post('/createurs/destroy/{id}', 'CreateurController@destroy')->name('createurs.destroy');

Route::get('/stocks', 'StockController@index')->name('stocks');
Route::get('/stocks/create', 'StockController@create')->name('stocks.create');
Route::post('/stocks/store', 'StockController@store')->name('stocks.store');
Route::get('/stocks/edit/{id}', 'StockController@edit')->name('stocks.edit');
Route::post('/stocks/update/{id}', 'StockController@update')->name('stocks.update');
Route::get('/stocks/delete/{id}', 'StockController@delete')->name('stocks.delete');
Route::post('/stocks/destroy/{id}', 'StockController@destroy')->name('stocks.destroy');

Route::get('/salles', 'SalleController@index')->name('salles');
Route::get('/salles/create', 'SalleController@create')->name('salles.create');
Route::post('/salles/store', 'SalleController@store')->name('salles.store');
Route::get('/salles/edit/{id}', 'SalleController@edit')->name('salles.edit');
Route::post('/salles/update/{id}', 'SalleController@update')->name('salles.update');
Route::get('/salles/delete/{id}', 'SalleController@delete')->name('salles.delete');
Route::post('/salles/destroy/{id}', 'SalleController@destroy')->name('salles.destroy');
Route::get('/salles/configurate/{id}', 'SalleController@configurate')->name('salles.configurate');

Route::get('/objets', 'ObjetController@index')->name('objets');
Route::post('/objets/save', 'ObjetController@save')->name('objets.save');
Route::get('/objets/delete/{id}', 'ObjetController@suppress')->name('objets.destroySalle');
