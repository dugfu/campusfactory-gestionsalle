<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Salle;

$factory->define(Salle::class, function (Faker $faker) {
    return [
        'idCreateur' => $faker->numberBetween(1,10),
        'nom' => $faker->name,
        'longueur' => $faker->numberBetween(1,100),
        'largeur' => $faker->numberBetween(1,100)
    ];
});
