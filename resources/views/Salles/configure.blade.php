@extends('layout')

@section('title','Salles')

@section('content')
    <?php
        $longueur = $salle->longueur;
        $largeur = $salle->largeur;
        $ratio = $largeur / $longueur;
    ?>
    <div class="row">
        <div class="col-2">
            <p>
                <u>ID :</u>&nbsp;{{ $salle->id }}<br/>
                <u>ID Créateur :</u>&nbsp; {{ $salle->idCreateur }}<br/>
                <u>Nom :</u>&nbsp;{{ $salle->nom }}<br/>
                <u>Longueur :</u>&nbsp;{{ $salle->longueur }}m<br/>
                <u>Largeur :</u>&nbsp;{{ $salle->largeur }}m<br/>
            </p>
        </div>
        <div class="col-8" align="center">
            <svg width="750" height="{{ 750 * $ratio }}" id="salleSVG">
                <defs>
                    <pattern id="trama" patternUnits="objectBoundingBox" width="2.5%" height="2.5%" patternTransform="rotate(25)" >
                        <line id="linea" y2="1%" />
                    </pattern>
                </defs>

            </svg>
        </div>
        <div class="col-2">
            <ul class="list-group" style="height: 400px;overflow-y: auto;">
            @foreach($stocks as $stock)
                    <li class="list-group-item" style="padding: 0px;border: 0px solid transparent;">
                        <div class="btn-group" style="width: 100%;" >
                            <button
                                type="button"
                                class="btn btn-light"
                                style="width: 80%;border-radius: 0px;"
                                onclick="ShowProperty({{$stock}}, 0)"
                            >
                                {{ $stock->nom }}
                            </button>
                            <button
                                class="btn btn-success"
                                style="width: 20%;border-radius: 0px;"
                                onclick="insertObject({{ 750 * ($stock->longueur / $longueur) }}, {{ 750 * ($stock->largeur / $longueur) }}, {{ $stock }})"
                            >
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </li>
            @endforeach
            </ul>
            <div id="accordion">
                <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                            Propriétés
                        </a>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                        <div class="card-body" id="property">
                            Pas d'objets selectionnés
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
