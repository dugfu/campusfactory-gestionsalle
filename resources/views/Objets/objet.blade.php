@extends('layout')

@section('title','Objets')

@section('content')

    <table class="table table-dark table-striped p-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>IDSalle</th>
            <th>Nom</th>
            <th>Longueur</th>
            <th>Largeur</th>
            <th>Rotation</th>
            <th>PositionX</th>
            <th>PositionY</th>
            <th>Place</th>
            <th>SVG</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($objets as $objet)
            <tr>
                <td>{{ $objet->id }}</td>
                <td>{{ $objet->idSalle }}</td>
                <td>{{ $objet->nom }}</td>
                <td>{{ $objet->longueur }}</td>
                <td>{{ $objet->largeur }}</td>
                <td>{{ $objet->rotation }}</td>
                <td>{{ $objet->positionX }}</td>
                <td>{{ $objet->positionY }}</td>
                <td>{{ $objet->place }}</td>
                <td>{{ $objet->SVG }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

