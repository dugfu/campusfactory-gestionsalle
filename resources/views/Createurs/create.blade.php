@extends('layout')

@section('title','Créateurs')

@section('content')
    <form method="post" action="{{ route('createurs.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" name="nom" class="form-control" placeholder="Entrer nom" id="nom" required>
        </div>
        <div class="form-group">
            <label for="prenom">Prénom :</label>
            <input type="text" name="prenom" class="form-control" placeholder="Entrer prénom" id="prenom" required>
        </div>
        <div class="form-group">
            <label for="pseudo">Pseudo :</label>
            <input type="text" name="pseudo" class="form-control" placeholder="Entrer pseudo" id="pseudo" required>
        </div>
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
    </form>
@endsection
